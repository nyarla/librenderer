package librenderer

// M is a alias of `map[string]interface{}`
type M map[string]interface{}
type FuncMap map[string]interface{}

type Renderer interface {
	Render(name string, vars M) ([]byte, error)
}

type RendererFunc func(name string, vars M) ([]byte, error)

func (r RendererFunc) Render(name string, vars M) ([]byte, error) {
	return r(name, vars)
}
