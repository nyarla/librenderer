package librenderer

import (
	"reflect"
	"testing"
)

func TestRenderer(t *testing.T) {
	testFn := func(name string, vars M) ([]byte, error) {
		return []byte(name + `:` + vars[`name`].(string)), nil
	}

	renderer := RendererFunc(testFn)

	if ret, err := renderer.Render(`hello`, M{`name`: `nyarla`}); err != nil {
		t.Fatal(err)
	} else {
		if !reflect.DeepEqual(ret, []byte(`hello:nyarla`)) {
			t.Fatal(`assertion failed for Renderer.Render`)
		}
	}
}
