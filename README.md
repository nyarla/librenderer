librenderer
===========

  * A renderer utility for golang

Install
-------

```
$ go get -u github.com/nyarla/librenderer
```

API Documentation
-----------------

  * [![GoDoc](http://godoc.org/github.com/nyarla/librenderer?status.svg)](https://godoc.org/github.com/nyarla/librenderer)

CI Status
---------

  * [![Build Status](https://travis-ci.org/nyarla/librenderer.svg?branch=master)](https://travis-ci.org/nyarla/librenderer)

Author
------

  * Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

LICENSE
-------

  * MIT
