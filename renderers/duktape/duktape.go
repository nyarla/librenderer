package duktape

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	"github.com/nyarla/librenderer"
	"gopkg.in/olebedev/go-duktape.v2"
)

type DuktapeRenderer struct {
	Assets    librenderer.Assets
	AssetName string

	vm   *duktape.Context
	lock *sync.Mutex
}

func (r *DuktapeRenderer) Render(name string, vars librenderer.M) ([]byte, error) {
	if r.lock == nil {
		r.lock = new(sync.Mutex)
	}

	r.lock.Lock()
	defer r.lock.Unlock()

	if r.vm == nil {
		script, err := r.Assets.Asset(r.AssetName)
		if err != nil {
			return []byte{}, err
		}

		r.vm = duktape.New()

		if err := r.vm.PevalString(string(script)); err != nil {
			return []byte{}, err
		}
	}

	data, err := json.Marshal(map[string]interface{}(vars))
	if err != nil {
		return []byte{}, err
	}

	var retcode int
	var message string

	r.vm.PushGlobalObject()
	if r.vm.GetPropString(-1, name) {
		r.vm.PevalString(fmt.Sprintf(`(%s)`, string(data)))
		retcode = r.vm.Pcall(1)
		message = r.vm.SafeToString(-1)
	} else {
		retcode = -1
		message = `failed to get function object for this renderer from duktape's global context.`
	}

	r.vm.Pop()

	if retcode != 0 {
		return []byte{}, errors.New(message)
	}

	return []byte(message), nil
}

func (r *DuktapeRenderer) Close() error {
	r.vm.Destroy()

	return nil
}
