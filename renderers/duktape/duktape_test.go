package duktape

import (
	"github.com/nyarla/librenderer"

	"testing"
)

func TestDuktapeRenderer(t *testing.T) {
	assets := librenderer.AssetsFunc(func(name string) ([]byte, error) {
		return []byte(`
			function renderString(props) {
				return "hello, " + props.name ;
			}
		`), nil
	})

	r := &DuktapeRenderer{
		Assets:    assets,
		AssetName: ``,
	}

	defer r.Close()

	if ret, err := r.Render(`renderString`, librenderer.M{`name`: `nyarla`}); err != nil {
		t.Fatal(err)
	} else {
		if string(ret) != `hello, nyarla` {
			t.Fatal(`assert failed for renderer result: `, string(ret))
		}
	}
}
