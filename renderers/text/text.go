package text

import (
	"bytes"
	"sync"
	"text/template"

	"github.com/nyarla/librenderer"
)

type TextTemplateRenderer struct {
	Assets librenderer.Assets
	Funcs  librenderer.FuncMap

	memlock  *sync.Mutex
	memcache map[string]*template.Template
}

func (r *TextTemplateRenderer) Render(name string, vars librenderer.M) ([]byte, error) {
	if r.memcache == nil {
		r.memcache = make(map[string]*template.Template)
	}

	if r.memlock == nil {
		r.memlock = new(sync.Mutex)
	}

	r.memlock.Lock()
	defer r.memlock.Unlock()

	tpl, exists := r.memcache[name]
	if !exists {
		src, err := r.Assets.Asset(name)
		if err != nil {
			return []byte{}, err
		}

		tpl, err = template.New(name).Parse(string(src))
		if err != nil {
			return []byte{}, err
		}

		if r.Funcs != nil {
			tpl = tpl.Funcs(template.FuncMap(r.Funcs))
		}

		r.memcache[name] = tpl
	}

	b := new(bytes.Buffer)
	if err := tpl.Execute(b, vars); err != nil {
		return []byte{}, err
	}

	return b.Bytes(), nil
}
