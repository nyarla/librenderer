package html

import (
	"testing"

	"github.com/nyarla/librenderer"
)

func TestHTMLTemplateRenderer(t *testing.T) {
	a := librenderer.AssetsFunc(func(name string) ([]byte, error) {
		return []byte(`<p>` + name + ` {{ .Name }}</p>`), nil
	})
	r := &HTMLTemplateRenderer{Assets: a}

	if ret, err := r.Render(`hello`, librenderer.M{`Name`: `<i>nyarla</i>`}); err != nil {
		t.Fatal(err)
	} else {
		if string(ret) != `<p>hello &lt;i&gt;nyarla&lt;/i&gt;</p>` {
			t.Fatal(`failed to render html template `, string(ret))
		}
	}
}
