package librenderer

import (
	"reflect"
	"testing"
)

func TestAssets(t *testing.T) {
	testFn := func(name string) ([]byte, error) {
		return []byte(name), nil
	}

	assets := AssetsFunc(testFn)

	if ret, err := assets.Asset(`foo`); err != nil {
		t.Fatal(err)
	} else {
		if !reflect.DeepEqual(ret, []byte(`foo`)) {
			t.Fatal(`assertion failed on Assets.Asset.`)
		}
	}
}
