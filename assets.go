package librenderer

type Assets interface {
	Asset(name string) ([]byte, error)
}

type AssetsFunc func(name string) ([]byte, error)

func (a AssetsFunc) Asset(name string) ([]byte, error) {
	return a(name)
}
